#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include <ImfRgba.h>
#include <ImfRgbaFile.h>

namespace py = pybind11;

using Real = double;

py::array_t<Real> readEXR(const std::string &name) {
    using namespace Imf;
    using namespace Imath;

    RgbaInputFile file(name.c_str());
    Box2i dw = file.dataWindow();

    // OpenEXR uses inclusive pixel bounds; adjust to non-inclusive
    // (the convention pbrt uses) in the values returned.
    int width = dw.max.x - dw.min.x + 1;
    int height = dw.max.y - dw.min.y + 1;
    std::vector<Rgba> pixels(width * height);
    file.setFrameBuffer(&pixels[0] - dw.min.x - dw.min.y * width, 1,
                        width);
    file.readPixels(dw.min.y, dw.max.y);

    py::array_t<Real> ret = py::array_t<Real>({height, width, 3});
    auto acc = ret.mutable_unchecked<3>();
    int index = 0;
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            acc(y, x, 0) = pixels[index].r;
            acc(y, x, 1) = pixels[index].g;
            acc(y, x, 2) = pixels[index].b;
            index++;
        }
    }
    return ret;
}

void writeEXR(const std::string &name, const py::array_t<Real> &img) {
    using namespace Imf;
    using namespace Imath;

    int xRes = img.shape()[1];
    int yRes = img.shape()[0];
    int totalXRes = xRes;
    int totalYRes = yRes;
    int xOffset = 0;
    int yOffset = 0;

    auto pixels = img.unchecked<3>();

    Rgba *hrgba = new Rgba[xRes * yRes];
    int index = 0;
    for (int y = 0; y < img.shape()[0]; y++) {
        for (int x = 0; x < img.shape()[1]; x++) {
            hrgba[index++] = Rgba(
                pixels(y, x, 0), pixels(y, x, 1), pixels(y, x, 2));
        }
    }

    // OpenEXR uses inclusive pixel bounds.
    Box2i displayWindow(V2i(0, 0), V2i(totalXRes - 1, totalYRes - 1));
    Box2i dataWindow(V2i(xOffset, yOffset),
                     V2i(xOffset + xRes - 1, yOffset + yRes - 1));

    RgbaOutputFile file(name.c_str(), displayWindow, dataWindow,
                        WRITE_RGBA);
    file.setFrameBuffer(hrgba - xOffset - yOffset * xRes, 1, xRes);
    file.writePixels(yRes);

    delete[] hrgba;
}

PYBIND11_MODULE(dopenexr, m) {
    m.def("readEXR", &readEXR, "");
    m.def("writeEXR", &writeEXR, "");
}
