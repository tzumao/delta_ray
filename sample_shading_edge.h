#pragma once

#include "delta_ray.h"
#include "vector.h"
#include "intersect.h"

struct ShadingEdgeSampleResult {
    Vector3 dp = Vector3{0.f, 0.f, 0.f};
    Vector3 dv0 = Vector3{0.f, 0.f, 0.f};
    Vector3 dv1 = Vector3{0.f, 0.f, 0.f};
};

ShadingEdgeSampleResult sample_shading_edge(
	const Scene &scene,
	const EdgeSample &edge_sample,
	const Edge &edge,
	const LightSample &light_sample,
	const std::vector<SecondarySample> &secondary_samples,
	const Vector3 &wi,
	const Intersection &shading_isect,
	const SurfacePoint &shading_point,
	const Vector3 &weight,
	const Vector3 &d_color,
	bool direct_only);