#pragma once

#include "delta_ray.h"
#include "vector.h"
#include "edge.h"

struct PrimaryEdgeSampleResult {
    Real screen_x = -1.f, screen_y = -1.f;
    Edge edge = Edge{};
    Vector3 dv0 = Vector3{0.f, 0.f, 0.f};
    Vector3 dv1 = Vector3{0.f, 0.f, 0.f};
    Vector2 dv0_ss = Vector2{0.f, 0.f};
    Vector2 dv1_ss = Vector2{0.f, 0.f};
    Matrix4x4 d_cam_to_sample, d_world_to_cam;
};

PrimaryEdgeSampleResult sample_primary_edge(
    const Scene &scene,
    const EdgeSample &edge_sample,
    const Edge &edge,
    const LightSample &light_sample,
    const std::vector<SecondarySample> &secondary_samples,
    const py::array_t<Real> &d_image,
    Real weight);