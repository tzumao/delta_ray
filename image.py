import numpy as np
import delta_ray
import Imath
import skimage
import skimage.io
import imageio
import os
import dopenexr

def imwrite(img, filename):
    dir = os.path.dirname(filename)
    if dir != '' and not os.path.exists(dir):
        os.makedirs(dir)
    if (filename[-4:] == '.exr'):
        dopenexr.writeEXR(filename, img)
    else:
        skimage.io.imsave(filename, np.power(np.clip(img, 0.0, 1.0), 1.0/2.2))

def imread(filename):
    if (filename[-4:] == '.exr'):
        return dopenexr.readEXR(filename)
    elif (filename[-4:] == '.hdr'):
        return imageio.imread(filename)
    else:
        im = skimage.io.imread(filename)
        if im.ndim == 2:
            im = np.stack([im, im, im], axis=-1)
        elif im.shape[2] == 4:
            im = im[:, :, :3]
        return np.power(\
            skimage.img_as_float(im).astype(np.float32), 2.2)
