#include "sample_primary_edge.h"
#include "scene.h"
#include "light.h"
#include "pathtrace.h"
#include "transform.h"
#include "camera.h"

PrimaryEdgeSampleResult sample_primary_edge(
        const Scene &scene,
        const EdgeSample &edge_sample,
        const Edge &edge,
        const LightSample &light_sample,
        const std::vector<SecondarySample> &secondary_samples,
        const py::array_t<Real> &d_image,
        Real weight) {
    // Project the edge onto screen space
    auto v0 = get_v0(edge);
    auto v1 = get_v1(edge);
    auto proj = project(scene.camera, v0, v1);
    if (!proj) {
        // Both points are behind the camera
        return PrimaryEdgeSampleResult{};
    }
    auto v0_ss = std::get<0>(*proj);
    auto v1_ss = std::get<1>(*proj);
    // "Unproject" to camera space
    auto v0_dir = unproject(scene.camera, v0_ss);
    auto v1_dir = unproject(scene.camera, v1_ss);
    assert(isfinite(v0_dir) && isfinite(v1_dir));
    // Pick a point on the edge
    auto v_dir3 = v1_dir - v0_dir;
    auto t = edge_sample.u;
    auto v0_local = xfm_point(scene.camera.world_to_cam, v0);
    auto v1_local = xfm_point(scene.camera.world_to_cam, v1);
    auto edge_pt3 = v0_dir + t * v_dir3;
    auto edge_local = v0_local + t * v1_local;
    // Reject samples outside of image plane
    // TODO: a smarter sampling strategy is to not sample these in the first place
    auto edge_pt = project_local(scene.camera, edge_pt3);
    assert(isfinite(edge_pt));
    if (!in_screen(scene.camera, edge_pt)) {
        return PrimaryEdgeSampleResult{};
    }
    auto xi = int(edge_pt[0] * d_image.shape()[1]);
    auto yi = int(edge_pt[1] * d_image.shape()[0]);
    auto d_img_accessor = d_image.unchecked<3>();
    auto d_color = Vector3{
        d_img_accessor(yi, xi, 0),
        d_img_accessor(yi, xi, 1),
        d_img_accessor(yi, xi, 2)};

    // Compute boundary difference by shading twice
    auto delta = 1e-9f * length(edge_local);
    auto split_plane_normal = normalize(cross(v0_dir, v1_dir));
    // Sample two rays on the two sides of the edge
    auto right_dir = normalize(edge_pt3 + delta * split_plane_normal);
    auto right_pt = project_local(scene.camera, right_dir);
    auto right_ray = sample_primary(scene.camera, right_pt);
    auto right_isect_result = intersect(scene, right_ray);
    auto left_dir = normalize(edge_pt3 - delta * split_plane_normal);
    auto left_pt = project_local(scene.camera, left_dir);
    auto left_ray = sample_primary(scene.camera, left_pt);
    auto left_isect_result = intersect(scene, left_ray);
    // At least one of the intersections should be connected to the edge
    auto isect_right = std::get<0>(right_isect_result);
    auto isect_left = std::get<0>(left_isect_result);
    bool right_connected = isect_right.shape == edge.shape &&
        (isect_right.tri_id == edge.f0 || isect_right.tri_id == edge.f1);
    bool left_connected = isect_left.shape == edge.shape &&
        (isect_left.tri_id == edge.f0 || isect_left.tri_id == edge.f1);
    if (!right_connected && !left_connected) {
        return PrimaryEdgeSampleResult{};
    }
    auto right_color = shade(scene,
                             -right_ray.dir,
                             std::get<0>(right_isect_result),
                             std::get<1>(right_isect_result),
                             light_sample,
                             secondary_samples,
                             true);
    auto left_color = shade(scene,
                            -left_ray.dir,
                            std::get<0>(left_isect_result),
                            std::get<1>(left_isect_result),
                            light_sample,
                            secondary_samples,
                            true);
    auto diff = (right_color - left_color) * weight;
    auto xdiff = diff;
    //if (left_pt[0] > right_pt[0]) {
    //    xdiff = -xdiff;
    //}
    auto ydiff = diff;
    //if (left_pt[1] > right_pt[1]) {
    //    ydiff = -ydiff;
    //}
    //auto xdiff = diff;
    //auto ydiff = diff;
    auto edge_dir = unproject(scene.camera, edge_pt);
    auto d_v0_dir = d_unproject(scene.camera, v0_ss);
    auto d_v1_dir = d_unproject(scene.camera, v1_ss);
    auto d_edge_dir = d_unproject(scene.camera, edge_pt);
    // alpha(x, y) = dot(edge_dir(x, y), cross(v0_dir, v1_dir))
    // d alpha(x, y)/dx = dot(d/dx edge_dir(x, y),  cross(v0_dir, v1_dir))
    // d alpha(x, y)/d v0_ss_x = dot(cross(v1_dir, edge_dir), d_unproject(v0_ss).x)
    auto d_alpha_dx = dot(std::get<0>(d_edge_dir), cross(v0_dir, v1_dir));
    auto d_alpha_dy = dot(std::get<1>(d_edge_dir), cross(v0_dir, v1_dir));
    auto d_alpha_d_v0_ss_x = (dot(cross(v1_dir, edge_dir), std::get<0>(d_v0_dir)));
    auto d_alpha_d_v0_ss_y = (dot(cross(v1_dir, edge_dir), std::get<1>(d_v0_dir)));
    auto d_alpha_d_v1_ss_x = (dot(cross(edge_dir, v0_dir), std::get<0>(d_v1_dir)));
    auto d_alpha_d_v1_ss_y = (dot(cross(edge_dir, v0_dir), std::get<1>(d_v1_dir)));
    auto dirac_jacobian =
        1.f / sqrt(d_alpha_dx * d_alpha_dx + d_alpha_dy * d_alpha_dy);
    auto edge_pt3_delta = v0_dir + (t + delta) * v_dir3;
    auto edge_pt_delta = project_local(scene.camera, edge_pt3_delta);
    auto line_jacobian = length((edge_pt_delta - edge_pt) / delta);
    auto jacobian = line_jacobian * dirac_jacobian;

    auto dv0_ss_x = dot(xdiff * d_alpha_d_v0_ss_x * jacobian, d_color);
    auto dv0_ss_y = dot(ydiff * d_alpha_d_v0_ss_y * jacobian, d_color);
    auto dv1_ss_x = dot(xdiff * d_alpha_d_v1_ss_x * jacobian, d_color);
    auto dv1_ss_y = dot(ydiff * d_alpha_d_v1_ss_y * jacobian, d_color);
    assert(isfinite(xdiff) && isfinite(ydiff));
    assert(isfinite(dv0_ss_x) && isfinite(dv0_ss_y) &&
    	   isfinite(dv1_ss_x) && isfinite(dv1_ss_y));

    // Need to propagate from screen space to world space
    // Use finite difference to obtain the Jacobian
    // TODO: replace this with closed-form later
    auto fd_delta = Real(1e-3f);
    auto proj_v0x = project(scene.camera, v0 + Vector3{fd_delta, 0.f, 0.f}, v1);
    auto proj_v0y = project(scene.camera, v0 + Vector3{0.f, fd_delta, 0.f}, v1);
    auto proj_v0z = project(scene.camera, v0 + Vector3{0.f, 0.f, fd_delta}, v1);
    auto proj_v1x = project(scene.camera, v0, v1 + Vector3{fd_delta, 0.f, 0.f});
    auto proj_v1y = project(scene.camera, v0, v1 + Vector3{0.f, fd_delta, 0.f});
    auto proj_v1z = project(scene.camera, v0, v1 + Vector3{0.f, 0.f, fd_delta});
    if (!proj_v0x || !proj_v0y || !proj_v0z || !proj_v1x || !proj_v1y || !proj_v1z) {
        // Numerical issue of finite difference, just abort operation.
        // This should be rare
        return PrimaryEdgeSampleResult{};
    }
    auto v0ss_delta_v0x = std::get<0>(*proj_v0x);
    auto v1ss_delta_v0x = std::get<1>(*proj_v0x);
    auto v0ss_delta_v0y = std::get<0>(*proj_v0y);
    auto v1ss_delta_v0y = std::get<1>(*proj_v0y);
    auto v0ss_delta_v0z = std::get<0>(*proj_v0z);
    auto v1ss_delta_v0z = std::get<1>(*proj_v0z);
    auto v0ss_delta_v1x = std::get<0>(*proj_v1x);
    auto v1ss_delta_v1x = std::get<1>(*proj_v1x);
    auto v0ss_delta_v1y = std::get<0>(*proj_v1y);
    auto v1ss_delta_v1y = std::get<1>(*proj_v1y);
    auto v0ss_delta_v1z = std::get<0>(*proj_v1z);
    auto v1ss_delta_v1z = std::get<1>(*proj_v1z);
    auto dv0ss_dv0x = (v0ss_delta_v0x - v0_ss) / fd_delta;
    auto dv0ss_dv0y = (v0ss_delta_v0y - v0_ss) / fd_delta;
    auto dv0ss_dv0z = (v0ss_delta_v0z - v0_ss) / fd_delta;
    auto dv0ss_dv1x = (v0ss_delta_v1x - v0_ss) / fd_delta;
    auto dv0ss_dv1y = (v0ss_delta_v1y - v0_ss) / fd_delta;
    auto dv0ss_dv1z = (v0ss_delta_v1z - v0_ss) / fd_delta;
    auto dv1ss_dv0x = (v1ss_delta_v0x - v1_ss) / fd_delta;
    auto dv1ss_dv0y = (v1ss_delta_v0y - v1_ss) / fd_delta;
    auto dv1ss_dv0z = (v1ss_delta_v0z - v1_ss) / fd_delta;
    auto dv1ss_dv1x = (v1ss_delta_v1x - v1_ss) / fd_delta;
    auto dv1ss_dv1y = (v1ss_delta_v1y - v1_ss) / fd_delta;
    auto dv1ss_dv1z = (v1ss_delta_v1z - v1_ss) / fd_delta;
    auto dv0 = Vector3{
        dv0_ss_x * dv0ss_dv0x[0] + dv0_ss_y * dv0ss_dv0x[1] +
        dv1_ss_x * dv1ss_dv0x[0] + dv1_ss_y * dv1ss_dv0x[1],
        dv0_ss_x * dv0ss_dv0y[0] + dv0_ss_y * dv0ss_dv0y[1] +
        dv1_ss_x * dv1ss_dv0y[0] + dv1_ss_y * dv1ss_dv0y[1],
        dv0_ss_x * dv0ss_dv0z[0] + dv0_ss_y * dv0ss_dv0z[1] +
        dv1_ss_x * dv1ss_dv0z[0] + dv1_ss_y * dv1ss_dv0z[1]};
    auto dv1 = Vector3{
        dv0_ss_x * dv0ss_dv1x[0] + dv0_ss_y * dv0ss_dv1x[1] +
        dv1_ss_x * dv1ss_dv1x[0] + dv1_ss_y * dv1ss_dv1x[1],
        dv0_ss_x * dv0ss_dv1y[0] + dv0_ss_y * dv0ss_dv1y[1] +
        dv1_ss_x * dv1ss_dv1y[0] + dv1_ss_y * dv1ss_dv1y[1],
        dv0_ss_x * dv0ss_dv1z[0] + dv0_ss_y * dv0ss_dv1z[1] +
        dv1_ss_x * dv1ss_dv1z[0] + dv1_ss_y * dv1ss_dv1z[1]};
    assert(isfinite(dv0) && isfinite(dv1));

    Matrix4x4 d_cam_to_sample, d_world_to_cam;
    d_project(scene.camera, v0, v1,
        dv0_ss_x, dv0_ss_y, dv1_ss_x, dv1_ss_y,
        d_cam_to_sample, d_world_to_cam);

    return PrimaryEdgeSampleResult{
        edge_pt[0], edge_pt[1],
        edge,
        dv0, dv1,
        Vector2{dv0_ss_x, dv0_ss_y},
        Vector2{dv1_ss_x, dv1_ss_y},
        d_cam_to_sample, d_world_to_cam};
}
