#include "sample_shading_edge.h"
#include "scene.h"
#include "edge.h"
#include "light.h"
#include "pathtrace.h"

ShadingEdgeSampleResult sample_shading_edge(
        const Scene &scene,
        const EdgeSample &edge_sample,
        const Edge &edge,
        const LightSample &light_sample,
        const std::vector<SecondarySample> &secondary_samples,
        const Vector3 &wi,
        const Intersection &shading_isect,
        const SurfacePoint &shading_point,
        const Vector3 &weight,
        const Vector3 &d_color,
        bool direct_only) {
    if (!shading_isect.valid()) {
        return ShadingEdgeSampleResult{};
    }
    if (!is_silhouette(shading_point.position, edge)) {
        return ShadingEdgeSampleResult{};
    }

    // Get the two vertices of the edge
    auto v0 = get_vertex(*edge.shape, edge.v0);
    auto v1 = get_vertex(*edge.shape, edge.v1);
    if (abs_sum(v1 - v0) == 1e-10f) {
        return ShadingEdgeSampleResult{};
    }

    const Material &material = *shading_isect.shape->material;

    // Importance sample the edge using linearly transformed cosine
    // First decide which component of BRDF to sample
    auto diffuse_reflectance =
        get_diffuse_reflectance(material, shading_point.uv);
    auto specular_reflectance =
        get_specular_reflectance(material, shading_point.uv);
    auto diffuse_weight = luminance(diffuse_reflectance);
    auto specular_weight = luminance(specular_reflectance);
    auto weight_sum = diffuse_weight + specular_weight;
    if (weight_sum <= 0.f) {
        // black BSDF?
        return ShadingEdgeSampleResult{};
    }
    auto diffuse_pmf = diffuse_weight / weight_sum;
    auto specular_pmf = specular_weight / weight_sum;
    auto m_pmf = 0.f;
    auto n = shading_point.shading_frame[2];
    auto frame_x = normalize(wi - n * dot(wi, n));
    auto frame_y = cross(n, frame_x);
    auto isotropic_frame = Frame{frame_x, frame_y, n};
    auto m = Matrix3x3{};
    auto m_inv = Matrix3x3{};
    if (edge_sample.bsdf_component <= diffuse_pmf) {
        // M is shading frame * identity
        m_inv = make_matrix3x3(isotropic_frame);
        m = inverse(m_inv);
        m_pmf = diffuse_pmf;
    } else {
        m_inv = inverse(get_ltc_matrix(material, shading_point, wi)) *
                make_matrix3x3(isotropic_frame);
        m = inverse(m_inv);
        // m = inverse(make_matrix3x3(shading_point.shading_frame)) *
        //     get_ltc_matrix(material, shading_point, wi);
        // m_inv = inverse(m);
        m_pmf = specular_pmf;
    }
    auto v0o = m_inv * (v0 - shading_point.position);
    auto v1o = m_inv * (v1 - shading_point.position);
    if (v0o[2] <= 0.f && v1o[2] <= 0.f) {
        // The integral is 0 (edge below horizon)
        // The derivatives are 0.
        return ShadingEdgeSampleResult{};
    }
    // Clip to the horizon
    if (v0o[2] < 0.f) {
        v0o = (v0o*v1o[2] - v1o*v0o[2]) / (v1o[2] - v0o[2]);
    }
    if (v1o[2] < 0.f) {
        v1o = (v0o*v1o[2] - v1o*v0o[2]) / (v1o[2] - v0o[2]);
    }
    assert(v0o[2] >= 0.f && v1o[2] >= 0.f);
    auto u = edge_sample.u;
    auto vodir = v1o - v0o;
    if (abs_sum(vodir) < 1e-10f) {
        return ShadingEdgeSampleResult{};
    }
    auto wt = normalize(vodir);
    auto l0 = dot(v0o, wt);
    auto l1 = dot(v1o, wt);
    assert(isfinite(v0o));
    assert(isfinite(v1o));
    assert(isfinite(wt));
    assert(isfinite(l0));
    assert(isfinite(l1));
    auto vo = v0o - l0 * wt;
    auto d = length(vo);
    auto I = [&](Real l) {
        return (l/(d*(d*d+l*l))+atan(l/d)/(d*d))*vo[2] +
               (l*l/(d*(d*d+l*l)))*wt[2];
    };
    auto Il0 = I(l0);
    auto Il1 = I(l1);
    auto normalization = Il1 - Il0;
    if (normalization <= 1e-10f) {
        return ShadingEdgeSampleResult{};
    }
    auto line_cdf = [&](Real l) {
        return (I(l)-Il0)/normalization;
    };
    auto line_pdf = [&](Real l) {
        auto dist_sq=d*d+l*l;
        return 2.f*d*(vo+l*wt)[2]/(normalization*dist_sq*dist_sq);
    };
    // Hybrid bisection & Newton iteration
    auto lb = l0;
    auto ub = l1;
    if (lb > ub) {
        std::swap(lb, ub);
    }
    auto l = 0.5f * (lb + ub);
    for (int it = 0; it < 20; it++) {
        if (!(l >= lb && l <= ub)) {
            l = 0.5f * (lb + ub);
        }
        auto value = line_cdf(l) - u;
        if (fabs(value) < 1e-5f || it == 19) {
            break;
        }
        // The derivative may not be entirely accurate,
        // but the bisection is going to handle this
        if (value > 0.f) {
            ub = l;
        } else {
            lb = l;
        }
        auto derivative = line_pdf(l);
        l -= value / derivative;
    }
    if (line_pdf(l) <= 0.f) {
        // Numerical issue
        return ShadingEdgeSampleResult{};
    }
    assert(!isnan(l));
    // Convert from l to position
    auto sample_p = m * (vo + l * wt);

    // shading_point.position, v0 and v1 forms a plane that split the space
    // into two parts
    auto split_plane_normal =
        normalize(cross(v0 - shading_point.position, v1 - shading_point.position));
    // Generate sample directions
    auto delta = 1e-4f * length(sample_p);
    auto sample_dir = normalize(sample_p);

    // Sample two rays on the two sides of the edge
    auto v_right_dir = normalize(sample_dir + delta * split_plane_normal);
    auto v_left_dir  = normalize(sample_dir - delta * split_plane_normal);
    // Intersect the world
    auto isect_right_result = intersect(scene,
        make_ray(shading_point.position, v_right_dir));
    auto isect_right = std::get<0>(isect_right_result);
    auto surface_point_right = std::get<1>(isect_right_result);
    auto isect_left_result  = intersect(scene,
        make_ray(shading_point.position, v_left_dir));
    auto isect_left = std::get<0>(isect_left_result);
    auto surface_point_left = std::get<1>(isect_left_result);
    // At least one of the intersections should be connected to the edge
    // this is just for speedup, if we remove this we should still get
    // the correct answer up to numerical error.
    bool right_connected =
        isect_right.shape == edge.shape &&
        (isect_right.tri_id == edge.f0 || isect_right.tri_id == edge.f1);
    bool left_connected =
        isect_left.shape == edge.shape &&
        (isect_left.tri_id == edge.f0 || isect_left.tri_id == edge.f1);
    if ((!right_connected && !left_connected) ||
            (right_connected && left_connected) ||
            (isect_right.shape == shading_isect.shape &&
                isect_right.tri_id == shading_isect.tri_id) || // self intersection
            (isect_left.shape == shading_isect.shape &&
                isect_left.tri_id == shading_isect.tri_id)) {
        return ShadingEdgeSampleResult();
    }
    // Reject samples where the unconnected face is closer than the connected ones
    // The connected face is supposed to "block" the unconnected one
    // (probably some numerical error)
    // if (right_connected && isect_left.valid()) {
    //    auto left_dir = surface_point_left.position - shading_point.position;
    //    auto left_distance_squared = length_squared(left_dir);
    //    auto right_dir = surface_point_right.position - shading_point.position;
    //    auto right_distance_squared = length_squared(right_dir);
    //    if (left_distance_squared < right_distance_squared) {
    //        return ShadingEdgeSampleResult();
    //    }
    // }
    // if (left_connected && isect_right.valid()) {
    //    auto left_dir = surface_point_left.position - shading_point.position;
    //    auto left_distance_squared = length_squared(left_dir);
    //    auto right_dir = surface_point_right.position - shading_point.position;
    //    auto right_distance_squared = length_squared(right_dir);
    //    if (right_distance_squared < left_distance_squared) {
    //        return ShadingEdgeSampleResult();
    //    }
    // }

    auto eval_bsdf = bsdf(material, shading_point, wi, sample_dir);
    if (sum(eval_bsdf) < 1e-6f) {
        return ShadingEdgeSampleResult();
    }
    auto shade_right = Vector3{0.f, 0.f, 0.f};
    auto shade_left = Vector3{0.f, 0.f, 0.f};
    if (!direct_only) {
        shade_right = shade(scene,
                            -sample_dir,
                            isect_right,
                            surface_point_right,
                            light_sample,
                            secondary_samples,
                            false);
        shade_left = shade(scene,
                           -sample_dir,
                           isect_left,
                           surface_point_left,
                           light_sample,
                           secondary_samples,
                           false);
        if (isect_right.valid()) {
            auto dir = surface_point_right.position - shading_point.position;
            auto distance_squared = length_squared(dir);
            if (distance_squared > 0.f) {
                // XXX HACK: clamp the maximum contribution of global illumination
                shade_right /= std::max(distance_squared, Real(1e-8f));
                shade_right *=
                    fabs(dot(normalize(dir), surface_point_right.geom_normal));
            } else {
                shade_right = Vector3{0.f, 0.f, 0.f};
            }
        }
        if (isect_left.valid()) {
            auto dir = surface_point_left.position - shading_point.position;
            auto distance_squared = length_squared(dir);
            if (distance_squared > 0.f) {
                // XXX HACK: clamp the maximum contribution of global illumination
                shade_left /= std::max(distance_squared, Real(1e-8f));
                shade_left *=
                    fabs(dot(normalize(dir), surface_point_left.geom_normal));
            } else {
                shade_left = Vector3{0.f, 0.f, 0.f};
            }
        }
    }
    // Evaluate the contribution of the two rays
    auto eval_right = 
        eval(scene, wi, shading_isect, shading_point,
             isect_right, surface_point_right,
             false) + eval_bsdf * shade_right;
    auto eval_left =
        eval(scene, wi, shading_isect, shading_point,
             isect_left, surface_point_left,
             false) + eval_bsdf * shade_left;
    // XXX ad-hoc clamping XXX
    // Fix this at some point
    // if (isect_right.valid()) {
    //     auto dir = surface_point_right.position - shading_point.position;
    //     auto distance = length(dir);
    //     auto cos_term = fabs(dot(normalize(dir), surface_point_right.geom_normal));
    //     if (distance * cos_term < 1e-3f) {
    //         return ShadingEdgeSampleResult();
    //     }
    // }
    // if (isect_left.valid()) {
    //     auto dir = surface_point_left.position - shading_point.position;
    //     auto distance = length(dir);
    //     auto cos_term = fabs(dot(normalize(dir), surface_point_left.geom_normal));
    //     if (distance * cos_term < 1e-3f) {
    //         return ShadingEdgeSampleResult();
    //     }
    // }

    assert(!isnan(eval_right) && !isnan(eval_left));
    auto dcolor_dp = Vector3{0.f, 0.f, 0.f};
    auto dcolor_dv0 = Vector3{0.f, 0.f, 0.f};
    auto dcolor_dv1 = Vector3{0.f, 0.f, 0.f};
    // Accumulate derivatives from two sides of the edge
    auto eval_edge = [&](const Vector3 &dir,
                         const Intersection &light_surface_isect,
                         const SurfacePoint &light_surface_point,
                         const Vector3 &eval) {
        // Jacobian from l to p = wt
        // Jacobian from p to Mp = M
        // Jacobian from Mp to dir
        // Jacobian from Mp to hit_pos
        auto line_dir = m * wt;
        auto isect_jacobian =
            intersect_jacobian(shading_point.position,
                               sample_p,
                               light_surface_point.position,
                               light_surface_point.geom_normal,
                               line_dir);

        // area of projection
        auto line_jacobian =
            (1.f / length(cross(light_surface_point.geom_normal, split_plane_normal))) *
            length(isect_jacobian) /
            (m_pmf * line_pdf(l));

        auto p = shading_point.position;
        auto x = light_surface_point.position;
        auto d0 = v0 - p;
        auto d1 = v1 - p;
        auto dirac_jacobian = length(cross(d0, d1));
        assert(isfinite(dirac_jacobian));
        auto w = line_jacobian / dirac_jacobian;
        assert(isfinite(w));

        auto dp = w * (cross(d1, d0) +
                       cross(x - p, d1) +
                       cross(d0, x - p));
        auto dv0 = w * cross(d1, x - p);
        auto dv1 = w * cross(x - p, d0);
        assert(isfinite(dp) && isfinite(dv0) && isfinite(dv1));
        auto weighted_eval = dot(eval * weight, d_color);
        dcolor_dp += dp * weighted_eval;
        dcolor_dv0 += dv0 * weighted_eval;
        dcolor_dv1 += dv1 * weighted_eval;
    };
    if (isect_right.valid() && abs_sum(eval_right) > 0.f) {
        eval_edge(v_right_dir, isect_right, surface_point_right, eval_right);
    }
    if (isect_left.valid() && abs_sum(eval_left) > 0.f) {
        eval_edge(v_left_dir, isect_left, surface_point_left, -eval_left);
    }
    assert(isfinite(dcolor_dp) && isfinite(dcolor_dv0) && isfinite(dcolor_dv1));

    return ShadingEdgeSampleResult{dcolor_dp, dcolor_dv0, dcolor_dv1};
}
