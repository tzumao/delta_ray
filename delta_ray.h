#pragma once

#ifndef __has_include
#define __has_include(header) 0
#endif

#if __has_include(<optional>)
#include <optional>
using std::optional;
using std::make_optional;
using std::nullopt;
#else
#include <experimental/optional>
using std::experimental::optional;
using std::experimental::make_optional;
using std::experimental::nullopt;
#endif

using Real = double;

struct Scene;
struct EdgeSample;
struct Edge;
struct LightSample;
struct SecondarySample;
struct Intersection;
