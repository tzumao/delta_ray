import torch
import torch.optim
from torch.autograd import Variable
import render_pytorch
import image
import camera
import material
import light
import shape
import numpy as np

resolution = [32, 32]
position = Variable(torch.from_numpy(np.array([0, 0, -5], dtype=np.float64)))
look_at = Variable(torch.from_numpy(np.array([0, 0, 0], dtype=np.float64)))
up = Variable(torch.from_numpy(np.array([0, 1, 0], dtype=np.float64)))
fov = Variable(torch.from_numpy(np.array([45.0], dtype=np.float64)))
clip_near = Variable(torch.from_numpy(np.array([0.01], dtype=np.float64)))
clip_far = Variable(torch.from_numpy(np.array([10000.0], dtype=np.float64)))
cam = camera.Camera(position     = position,
                    look_at      = look_at,
                    up           = up,
                    cam_to_world = None,
                    fov          = fov,
                    clip_near    = clip_near,
                    clip_far     = clip_far,
                    resolution   = resolution)
mat_green=material.Material(\
    diffuse_reflectance=torch.from_numpy(np.array([0.35,0.75,0.35],dtype=np.float64)))
mat_red=material.Material(\
    diffuse_reflectance=torch.from_numpy(np.array([0.75,0.35,0.35],dtype=np.float64)))
mat_black=material.Material(\
    diffuse_reflectance=torch.from_numpy(np.array([0.0,0.0,0.0],dtype=np.float64)))
materials=[mat_green,mat_red,mat_black]
tri0_vertices=Variable(torch.from_numpy(\
    np.array([[-1.7,1.0,0.0], [1.0,1.0,0.0], [-0.5,-1.0,0.0]],dtype=np.float64)))
tri1_vertices=Variable(torch.from_numpy(\
    np.array([[-1.0,1.5,1.0], [0.2,1.5,1.0], [0.2,-1.5,1.0]],dtype=np.float64)))
# tri0_vertices=Variable(torch.from_numpy(\
#     np.array([[-1.3,1.5,0.1], [1.5,0.7,-0.2], [-0.8,-1.1,0.2]],dtype=np.float32)),
#     requires_grad=True)
tri0_indices=torch.from_numpy(np.array([[0,1,2]],dtype=np.int32))
shape_tri0=shape.Shape(tri0_vertices,tri0_indices,None,None,0)
# tri1_vertices=Variable(torch.from_numpy(\
#     np.array([[-0.5,1.2,1.2], [0.3,1.7,1.0], [0.5,-1.8,1.3]],dtype=np.float32)),
#     requires_grad=True)
tri1_indices=torch.from_numpy(np.array([[0,1,2]],dtype=np.int32))
shape_tri1=shape.Shape(tri1_vertices,tri1_indices,None,None,1)
light_vertices=Variable(torch.from_numpy(\
    np.array([[-1,-1,-7],[1,-1,-7],[-1,1,-7],[1,1,-7]],dtype=np.float64)))
light_indices=torch.from_numpy(\
    np.array([[0,1,2],[1,3,2]],dtype=np.int32))
shape_light=shape.Shape(light_vertices,light_indices,None,None,2)
shapes=[shape_tri0,shape_tri1,shape_light]
light_intensity=torch.from_numpy(\
    np.array([20,20,20],dtype=np.float64))
light=light.Light(2,light_intensity)
lights=[light]

shape_tri0.vertices = Variable(torch.from_numpy(\
    np.array([[-1.3,1.5,0.1], [1.5,0.7,-0.2], [-0.8,-1.1,0.2]],dtype=np.float32)),
    requires_grad=True)
shape_tri1.vertices = Variable(torch.from_numpy(\
    np.array([[-0.5,1.2,1.2], [0.3,1.7,1.0], [0.5,-1.8,1.3]],dtype=np.float32)),
    requires_grad=True)
args=render_pytorch.RenderFunction.serialize_scene(\
    cam,materials,shapes,lights,resolution,1024,1,debug_id=0)
render = render_pytorch.RenderFunction.apply
img = render(2, *args)
loss = img.sum()
loss.backward()
print('grad:', shape_tri0.vertices.grad[0, 0].item())

eps = 1e-2
shape_tri0.vertices[0, 0] -= eps
args=render_pytorch.RenderFunction.serialize_scene(\
    cam,materials,shapes,lights,resolution,131072,1)
img_fwd = render(2, *args)
img_fwd = torch.sum(img_fwd, 2)
loss_fwd = img_fwd.sum()
shape_tri0.vertices[0, 0] += 2 * eps
args=render_pytorch.RenderFunction.serialize_scene(\
    cam,materials,shapes,lights,resolution,131072,1)
img_fwd_d = render(2, *args)
img_fwd_d = torch.sum(img_fwd_d, 2)
loss_fwd_d = img_fwd_d.sum()
image.imwrite(np.tile(np.expand_dims(img_fwd.data.numpy(), -1), (1, 1, 3)),
    'img_fwd.exr')
image.imwrite(np.tile(np.expand_dims(img_fwd_d.data.numpy(), -1), (1, 1, 3)),
    'img_fwd_d.exr')
dx = (img_fwd_d - img_fwd) / (2 * eps)
image.imwrite(np.tile(np.expand_dims(dx.data.numpy(), -1), (1, 1, 3)),
    'finite_dx.exr')
dx = -dx
image.imwrite(np.tile(np.expand_dims(dx.data.numpy(), -1), (1, 1, 3)),
    'inv_finite_dx.exr')
print('loss_fwd:', loss_fwd.item())
print('loss_fwd_d:', loss_fwd_d.item())
print('dx:', ((loss_fwd_d - loss_fwd) / (2 * eps)).item())
