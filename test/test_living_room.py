import load_mitsuba
import render_pytorch
import image
import transform
import torch
import torch.optim
from torch.autograd import Variable
import numpy as np
import camera
import scipy.ndimage.filters

max_bounces = 1
cam, materials, shapes, lights, resolution = \
	load_mitsuba.load_mitsuba('test/scenes/living-room-3/scene.xml')
args=render_pytorch.RenderFunction.serialize_scene(\
	cam, materials, shapes, lights, resolution, 1024, max_bounces)
render = render_pytorch.RenderFunction.apply
# img = render(0, *args)
# image.imwrite(img.data.numpy(), 'test/results/test_living_room/target.exr')
# image.imwrite(img.data.numpy(), 'test/results/test_living_room/target.png')
target = Variable(torch.from_numpy(image.imread('test/results/test_living_room/target.exr')))

cam.look_at = Variable(torch.from_numpy(\
    np.array([-0.556408, 0.951295, -3.98066], dtype=np.float64)),
    requires_grad=True)
cam.position = Variable(torch.from_numpy(\
    np.array([0.00419251, 0.973707, -4.80844], dtype=np.float64)),
    requires_grad=True)
cam.up = Variable(torch.from_numpy(\
    np.array([-0.00920347, 0.999741, 0.020835], dtype=np.float64)),
    requires_grad=True)
cam = camera.Camera(position     = cam.position,
                    look_at      = cam.look_at,
                    up           = cam.up,
                    cam_to_world = None,
                    fov          = cam.fov,
                    clip_near    = cam.clip_near,
                    clip_far     = cam.clip_far,
                    resolution   = resolution)
args=render_pytorch.RenderFunction.serialize_scene(\
    cam, materials, shapes, lights, resolution, 1024, max_bounces)
# img = render(1, *args)
# image.imwrite(img.data.numpy(), 'test/results/test_living_room/init.exr')
# image.imwrite(img.data.numpy(), 'test/results/test_living_room/init.png')
# diff = torch.abs(target - img)
# image.imwrite(diff.data.numpy(), 'test/results/test_living_room/init_diff.png')

# org_target = target
# optimizer = torch.optim.Adam([cam.position, cam.look_at, cam.up], lr = 1e-2)
# num_scales = 4
# for scale in range(num_scales):
#     # linearly scale from 32x32 to 256x256
#     downscale_factor = 256.0 / ((256.0 / num_scales) * scale + 32.0)
#     print('downscale_factor', downscale_factor)
#     res = round(256 / downscale_factor)
#     print('scaling target')
#     if scale < num_scales - 1:
#         target = scipy.ndimage.interpolation.zoom(org_target,
#             (1.0/downscale_factor, 1.0/downscale_factor, 1.0), order=1)
#     else:
#         downscale_factor = 1
#         res = 256
#         target = org_target
#     image.imwrite(target, 'test/results/test_living_room/target_{}.exr'.format(scale))
#     target = Variable(torch.from_numpy(target))
#     print('target.shape:', target.shape)
#     resolution = (res, res)
#     num_samples = round(4 * downscale_factor * downscale_factor)
#     for t in range(50):
#         optimizer.zero_grad()
#         cam = camera.Camera(position     = cam.position,
#                             look_at      = cam.look_at,
#                             up           = cam.up,
#                             cam_to_world = None,
#                             fov          = cam.fov,
#                             clip_near    = cam.clip_near,
#                             clip_far     = cam.clip_far,
#                             resolution   = resolution)

#         print('iteration:', t)
#         print('cam.look_at:', cam.look_at)
#         print('cam.position:', cam.position)
#         print('cam.up:', cam.up)
#         args=render_pytorch.RenderFunction.serialize_scene(\
#             cam, materials, shapes, lights, resolution, num_samples, max_bounces)

#         num_iter = scale * num_scales + t
#         img = render(num_iter + 1, *args)
#         image.imwrite(img.data.numpy(), 'test/results/test_living_room/iter_{}.png'.format(num_iter))
#         loss = (img - target).pow(2).sum()
#         print('loss:', loss.item())

#         loss.backward()
#         optimizer.step()

optimizer = torch.optim.Adam([cam.position, cam.look_at, cam.up], lr = 2e-2)
for t in range(200):
    optimizer.zero_grad()
    cam = camera.Camera(position     = cam.position,
                        look_at      = cam.look_at,
                        up           = cam.up,
                        cam_to_world = None,
                        fov          = cam.fov,
                        clip_near    = cam.clip_near,
                        clip_far     = cam.clip_far,
                        resolution   = resolution)

    print('iteration:', t)
    print('cam.look_at:', cam.look_at)
    print('cam.position:', cam.position)
    print('cam.up:', cam.up)
    args=render_pytorch.RenderFunction.serialize_scene(\
        cam, materials, shapes, lights, resolution, 4, max_bounces)

    img = render(t + 1, *args)
    image.imwrite(img.data.numpy(), 'test/results/test_living_room/iter_{}.png'.format(t))
    # loss = (img - target).pow(2).sum()

    dirac = np.zeros([7,7], dtype=np.float64)
    dirac[3,3] = 1.0
    dirac = Variable(torch.from_numpy(dirac))
    f = np.zeros([3, 3, 7, 7], dtype=np.float64)
    gf = scipy.ndimage.filters.gaussian_filter(dirac, 1.0)
    f[0, 0, :, :] = gf
    f[1, 1, :, :] = gf
    f[2, 2, :, :] = gf
    f = Variable(torch.from_numpy(f))
    m = torch.nn.AvgPool2d(2)

    res = 256
    diff_0 = (img - target).view(1, res, res, 3).permute(0, 3, 2, 1)
    diff_1 = m(torch.nn.functional.conv2d(diff_0, f, padding=3)) # 128 x 128
    diff_2 = m(torch.nn.functional.conv2d(diff_1, f, padding=3)) # 64 x 64
    diff_3 = m(torch.nn.functional.conv2d(diff_2, f, padding=3)) # 32 x 32
    diff_4 = m(torch.nn.functional.conv2d(diff_3, f, padding=3)) # 16 x 16
    loss = diff_0.pow(2).sum() / (res*res) + \
           diff_1.pow(2).sum() / ((res/2)*(res/2)) + \
           diff_2.pow(2).sum() / ((res/4)*(res/4)) + \
           diff_3.pow(2).sum() / ((res/8)*(res/8)) + \
           diff_4.pow(2).sum() / ((res/16)*(res/16))
    print('loss:', loss.item())

    loss.backward()
    optimizer.step()

    print('cam.look_at.grad:', cam.look_at.grad)
    print('cam.position.grad:', cam.position.grad)
    print('cam.up.grad:', cam.up.grad)

args=render_pytorch.RenderFunction.serialize_scene(\
    cam,materials,shapes,lights,resolution,1024,max_bounces)
img = render(202, *args)
image.imwrite(img.data.numpy(), 'test/results/test_living_room/final.exr')
image.imwrite(img.data.numpy(), 'test/results/test_living_room/final.png')
image.imwrite(np.abs(target.data.numpy() - final), 'test/results/test_living_room/final_diff.png')

from subprocess import call
call(["ffmpeg", "-framerate", "24", "-i",
    "test/results/test_living_room/iter_%d.png", "-vb", "20M", "test/results/test_living_room/out.mp4"])
