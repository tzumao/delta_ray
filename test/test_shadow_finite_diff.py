import torch
import torch.optim
from torch.autograd import Variable
import render_pytorch
import image
import camera
import material
import light
import shape
import numpy as np
import delta_ray

resolution = [32, 32]
position = Variable(torch.from_numpy(np.array([0, 2, -5], dtype=np.float64)))
look_at = Variable(torch.from_numpy(np.array([0, 0, 0], dtype=np.float64)))
up = Variable(torch.from_numpy(np.array([0, 1, 0], dtype=np.float64)))
fov = Variable(torch.from_numpy(np.array([45.0], dtype=np.float64)))
clip_near = Variable(torch.from_numpy(np.array([0.01], dtype=np.float64)))
clip_far = Variable(torch.from_numpy(np.array([10000.0], dtype=np.float64)))
cam = camera.Camera(position     = position,
                    look_at      = look_at,
                    up           = up,
                    cam_to_world = None,
                    fov          = fov,
                    clip_near    = clip_near,
                    clip_far     = clip_far,
                    resolution   = resolution)
mat_grey=material.Material(\
    diffuse_reflectance=torch.from_numpy(np.array([0.5,0.5,0.5],dtype=np.float64)))
mat_black=material.Material(\
    diffuse_reflectance=torch.from_numpy(np.array([0.0,0.0,0.0],dtype=np.float64)))
materials=[mat_grey,mat_black]
floor_vertices=Variable(torch.from_numpy(\
    np.array([[-2.0,0.0,-2.0],[-2.0,0.0,2.0],[2.0,0.0,-2.0],[2.0,0.0,2.0]],dtype=np.float64)))
floor_indices=torch.from_numpy(np.array([[0,1,2], [1,3,2]],dtype=np.int32))
shape_floor=shape.Shape(floor_vertices,floor_indices,None,None,0)
blocker_vertices=Variable(torch.from_numpy(\
    np.array([[-0.5,3.0,-0.5],[-0.5,3.0,0.5],[0.5,3.0,-0.5],[0.5,3.0,0.5]],dtype=np.float64)))
blocker_indices=torch.from_numpy(np.array([[0,1,2], [1,3,2]],dtype=np.int32))
shape_blocker=shape.Shape(blocker_vertices,blocker_indices,None,None,0)
light_vertices=Variable(torch.from_numpy(\
    np.array([[-0.1,7,-0.1],[-0.1,7,0.1],[0.1,5,-0.1],[0.1,5,0.1]],dtype=np.float64)))
light_indices=torch.from_numpy(\
    np.array([[0,2,1],[1,2,3]],dtype=np.int32))
shape_light=shape.Shape(light_vertices,light_indices,None,None,1)
shapes=[shape_floor,shape_blocker,shape_light]
light_intensity=torch.from_numpy(\
    np.array([1000,1000,1000],dtype=np.float64))
light=light.Light(2,light_intensity)
lights=[light]

render = render_pytorch.RenderFunction.apply
shape_blocker.vertices=Variable(torch.from_numpy(\
    np.array([[-0.2,3.5,-0.8],[-0.8,3.0,0.3],[0.4,2.8,-0.8],[0.3,3.2,1.0]],dtype=np.float64)),
    requires_grad=True)

args=render_pytorch.RenderFunction.serialize_scene(\
    cam,materials,shapes,lights,resolution,4096,1,debug_id=1)
img = render(2, *args)
loss = img.sum()
loss.backward()
print('grad:', shape_blocker.vertices.grad[0, 1].item())

eps = 1e-2
shape_blocker.vertices[0, 1] -= eps
args=render_pytorch.RenderFunction.serialize_scene(\
    cam,materials,shapes,lights,resolution,131072,1)
img_fwd = render(2, *args)
img_fwd = torch.sum(img_fwd, 2)
loss_fwd = img_fwd.sum()
shape_blocker.vertices[0, 1] += 2.0 * eps
args=render_pytorch.RenderFunction.serialize_scene(\
    cam,materials,shapes,lights,resolution,131072,1)
img_fwd_d = render(2, *args)
img_fwd_d = torch.sum(img_fwd_d, 2)
loss_fwd_d = img_fwd_d.sum()
image.imwrite(np.tile(np.expand_dims(img_fwd.data.numpy(), -1), (1, 1, 3)),
    'img_fwd.exr')
image.imwrite(np.tile(np.expand_dims(img_fwd_d.data.numpy(), -1), (1, 1, 3)),
    'img_fwd_d.exr')
dx = (img_fwd_d - img_fwd) / (2.0 * eps)
image.imwrite(np.tile(np.expand_dims(dx.data.numpy(), -1), (1, 1, 3)),
    'finite_dx.exr')
dx = -dx
image.imwrite(np.tile(np.expand_dims(dx.data.numpy(), -1), (1, 1, 3)),
    'inv_finite_dx.exr')
print('loss_fwd:', loss_fwd.item())
print('loss_fwd_d:', loss_fwd_d.item())
print('dx:', ((loss_fwd_d - loss_fwd) / (2.0 * eps)).item())
