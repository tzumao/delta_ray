import load_mitsuba
import render_pytorch
import image
import transform
import torch
import torch.optim
from torch.autograd import Variable
import numpy as np
import camera

cam, materials, shapes, lights, resolution = \
    load_mitsuba.load_mitsuba('test/scenes/teapot.xml')

materials[-1].diffuse_reflectance = \
    Variable(torch.from_numpy(np.array([0.3, 0.2, 0.2], dtype=np.float64)))
materials[-1].specular_reflectance = \
    Variable(torch.from_numpy(np.array([0.6, 0.6, 0.6], dtype=np.float64)))
materials[-1].roughness = \
    Variable(torch.from_numpy(np.array([0.05], dtype=np.float64)))
render = render_pytorch.RenderFunction.apply

cam_translation = Variable(torch.from_numpy(\
    cam.position.data.numpy()), requires_grad=True)
cam_look_at = Variable(torch.from_numpy(\
    cam.look_at.data.numpy() - cam.position.data.numpy()))

cam = camera.Camera(position     = cam_translation,
                    look_at      = cam_look_at + cam_translation,
                    up           = cam.up,
                    cam_to_world = None,
                    fov          = cam.fov,
                    clip_near    = cam.clip_near,
                    clip_far     = cam.clip_far,
                    resolution   = resolution,
                    fisheye      = False)

# shapes[0].vertices.requires_grad = True
# shapes[-1].vertices.requires_grad = True
# shapes[-2].vertices.requires_grad = True
args=render_pytorch.RenderFunction.serialize_scene(\
    cam,materials,shapes,lights,resolution,8192,1)
img = render(3, *args)
loss = img.sum()
loss.backward()
# print('grad:', cam_translation.grad[0].item())
# print('grad:', torch.sum(shapes[-1].vertices.grad[:, 2]).item() + \
#                torch.sum(shapes[-2].vertices.grad[:, 2]).item())
debug_img = render_pytorch.debug_img
image.imwrite(np.tile(np.expand_dims(debug_img, -1), (1, 1, 3)),
    'debug.exr')

eps = 1e-4
# cam_translation[0] -= eps
shapes[0].vertices[:, 2] -= eps
shapes[-1].vertices[:, 2] -= eps
shapes[-2].vertices[:, 2] -= eps
cam = camera.Camera(position     = cam_translation,
                    look_at      = cam_look_at + cam_translation,
                    up           = cam.up,
                    cam_to_world = None,
                    fov          = cam.fov,
                    clip_near    = cam.clip_near,
                    clip_far     = cam.clip_far,
                    resolution   = resolution,
                    fisheye      = False)
args=render_pytorch.RenderFunction.serialize_scene(\
    cam,materials,shapes,lights,resolution,262144,1)
img_fwd = render(3, *args)
# cam_translation[0] += 2.0 * eps
shapes[0].vertices[:, 2] += 2.0 * eps
shapes[-1].vertices[:, 2] += 2.0 * eps
shapes[-2].vertices[:, 2] += 2.0 * eps
cam = camera.Camera(position     = cam_translation,
                    look_at      = cam_look_at + cam_translation,
                    up           = cam.up,
                    cam_to_world = None,
                    fov          = cam.fov,
                    clip_near    = cam.clip_near,
                    clip_far     = cam.clip_far,
                    resolution   = resolution,
                    fisheye      = False)
args=render_pytorch.RenderFunction.serialize_scene(\
    cam,materials,shapes,lights,resolution,262144,1)
img_fwd_d = render(3, *args)

img_fwd = torch.sum(img_fwd, 2)
loss_fwd = img_fwd.sum()
img_fwd_d = torch.sum(img_fwd_d, 2)
loss_fwd_d = img_fwd_d.sum()

image.imwrite(np.tile(np.expand_dims(img_fwd.data.numpy(), -1), (1, 1, 3)),
    'img_fwd.exr')
image.imwrite(np.tile(np.expand_dims(img_fwd_d.data.numpy(), -1), (1, 1, 3)),
    'img_fwd_d.exr')
dx = (img_fwd_d - img_fwd) / (2.0 * eps)
image.imwrite(np.tile(np.expand_dims(dx.data.numpy(), -1), (1, 1, 3)),
    'finite_dx.exr')
# dx = -dx
# image.imwrite(np.tile(np.expand_dims(dx.data.numpy(), -1), (1, 1, 3)),
#     'inv_finite_dx.exr')

diff = np.absolute(debug_img - dx.data.numpy())
image.imwrite(np.tile(np.expand_dims(diff, -1), (1, 1, 3)),
    'diff.exr')
print('diff.sum():', diff.sum())
print('dx.sum():', np.absolute(dx.data.numpy()).sum())
print('diff.sum/dx.sum:', diff.sum() / np.absolute(dx.data.numpy()).sum())

# print('loss_fwd:', loss_fwd.item())
# print('loss_fwd_d:', loss_fwd_d.item())
# print('dx:', ((loss_fwd_d - loss_fwd) / (2.0 * eps)).item())
